#!/bin/bash
#wildfly_install.sh
#variables

SERVER_URL=http://172.16.16.104
BASE_DIR=/opt
TMP_DIR=/tmp

WILDFLY=wildfly
WILDFLY_FILENAME=wildfly-8.2.0.Final
WILDFLY_ARCHIVE_NAME=$WILDFLY_FILENAME.tar.gz


WILDFLY_BASE_DIR=$BASE_DIR/$WILDFLY_FILENAME
WILDFLY_DIR=$BASE_DIR/wildfly

WILDFLY_USER="wildfly"
WILDFLY_GROUP="wildfly"
WILDFLY_SERVICE="wildfly"

WILDFLY_STARTUP_TIMEOUT=240
WILDFLY_SHUTDOWN_TIMEOUT=30

WILDFLY_DOWNLOAD_ADDRESS=$SERVER_URL/$WILDFLY_ARCHIVE_NAME


#SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root."
   exit 1
fi

if [ -r /etc/init.d/$WILDFLY_SERVICE ]; then
  echo "Cleaning up"
    /etc/init.d/$WILDFLY_SERVICE stop
    rm -f "$WILDFLY_DIR"
    rm -rf "$WILDFLY_BASE_DIR"
    rm -f "/etc/default/$WILDFLY_SERVICE"
    rm -f "/etc/init.d/$WILDFLY_SERVICE"
    rm -f "/$TMP_DIR/$WILDFLY_ARCHIVE_NAME"
fi

echo "Downloading: $WILDFLY_DOWNLOAD_ADDRESS..."
[ -e "$WILDFLY_ARCHIVE_NAME" ] && echo 'Wildfly archive already exists.'
if [ ! -e "$WILDFLY_ARCHIVE_NAME" ]; then
  cd $TMP_DIR
  wget -q $WILDFLY_DOWNLOAD_ADDRESS
  if [ $? -ne 0 ]; then
    echo "Not possible to download Wildfly."
    exit 1
  fi
fi

echo "Install"
mkdir $WILDFLY_BASE_DIR
tar -xzf $WILDFLY_ARCHIVE_NAME -C $BASE_DIR
ln -s $WILDFLY_BASE_DIR/ $WILDFLY_DIR
addgroup $WILDFLY_GROUP
useradd -s /sbin/nologin  --no-create-home -g $WILDFLY_GROUP $WILDFLY_USER
chown -R $WILDFLY_USER:$WILDFLY_GROUP $WILDFLY_DIR
chown -R $WILDFLY_USER:$WILDFLY_GROUP $WILDFLY_BASE_DIR/

echo "Registrating service"
if [ -r /lib/lsb/init-functions ]; then
    ln -s $WILDFLY_DIR/bin/init.d/wildfly-init-debian.sh /etc/init.d/$WILDFLY_SERVICE
    ln -s $WILDFLY_DIR/bin/init.d/wildfly.conf /etc/default/$WILDFLY_SERVICE
    update-rc.d $WILDFLY_SERVICE  defaults
    WILDFLY_SERVICE_CONF=/etc/default/$WILDFLY_SERVICE
fi
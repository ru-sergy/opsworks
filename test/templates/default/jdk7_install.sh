#!/bin/bash
#jdk7_install.sh

#variables
SERVER_URL=http://172.16.16.104
BASE_DIR=/opt
TMP_DIR=/tmp
JDK_DIR=jdk
JDK7_ACHIVE_NAME=jdk-7u80-linux-x64.tar.gz
JDK7_NAME=jdk1.7.0_80

JDK_DOWNLOAD_ADDRESS=$SERVER_URL/$JDK7_ACHIVE_NAME

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root."
   exit 1
fi

echo "Downloading: $JDK_DOWNLOAD_ADDRESS..."
[ -e "$JDK7_ACHIVE_NAME" ] && echo 'JDK7 archive already exists.'
if [ ! -e "$JDK7_ACHIVE_NAME" ]; then
  cd $TMP_DIR
  wget -q $JDK_DOWNLOAD_ADDRESS
  if [ $? -ne 0 ]; then
    echo "Not possible to download JDK7."
    exit 1
  fi
fi

echo "Install"
tar -zxf $JDK7_ACHIVE_NAME -C $BASE_DIR
ln -s $BASE_DIR/$JDK7_NAME $BASE_DIR/$JDK_DIR

update-alternatives --install /usr/bin/java java $BASE_DIR/$JDK_DIR/bin/java 100
update-alternatives --install /usr/bin/javac javac $BASE_DIR/$JDK_DIR/bin/javac 100
echo 'JAVA_HOME="'$BASE_DIR/$JDK_DIR'"' >> /etc/environment
sed -i 's/games:\/usr/games:\/opt\/jdk\/bin:\/usr/g'  /etc/environment
#echo 'PATH="$JAVA_HOME/bin:$PATH"' >> /etc/environment
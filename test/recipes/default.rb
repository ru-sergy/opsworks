#
# Cookbook Name:: test
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
%w{ntp mc htop iotop iftop atop vim-common wget curl rkhunter git rcconf}.each do |packages|
    package packages do
    action :install
    end
end
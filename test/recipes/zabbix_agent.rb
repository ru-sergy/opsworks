#
# Cookbook Name:: Zabbix Install
# Recipe:: zabbix-agent
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
#Install Zabbix

package "zabbix-agent" do
  action :install
end

#Add zabbix agent to startup system
execute 'Add Zabbix to start' do
 command 'sudo update-rc.d zabbix-agent defaults'
 action :run
end
#
#Configure /etc/zabbix/zabbix_agentd.conf
#
template '/etc/zabbix/zabbix_agentd.conf' do
  source 'zabbix_agentd.conf.erb'
  mode '0644'
  action :create
end
#
execute 'add hos name' do
  command 'echo "Hostname =" $(cat /etc/hostname) >> /etc/zabbix/zabbix_agentd.conf'
  action :run
end
#Run zabbix agent
execute 'Add Zabbix to start' do
  command 'sudo /etc/init.d/zabbix-agent restart'
  action :run
end
#
#Done

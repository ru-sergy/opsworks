#
# Cookbook Name:: test
# Recipe:: jdk7
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
#require 'chef/log'
#Chef::Log.level = :debug

#delite result file
results = "/tmp/jdk7output.txt"
file results do
  action :delete
end

#copy install script
template '/tmp/jdk7_install.sh' do
  source 'jdk7_install.sh'
  mode '0755'
  action :create
end

#execute install script
execute 'Install JDK7' do
  command 'sudo /bin/bash -x /tmp/jdk7_install.sh 2> /tmp/jdk7output.txt'
  action :run
end

#bash resolts to console
ruby_block "Results" do
  only_if { ::File.exists?(results) }
  block do
    print "\n"
    File.open(results).each do |line|
      print line
    end
  end
end
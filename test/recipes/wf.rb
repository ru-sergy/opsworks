#
# Cookbook Name:: test
# Recipe:: wildfly
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
#delite result file
results = "/tmp/wfoutput.txt"
file results do
  action :delete
end

#copy install script
template '/tmp/wildfly_install.sh' do
  source 'wildfly_install.sh'
  mode '0755'
  action :create
end

#execute install script
execute 'Install JDK7' do
  command 'sudo /bin/bash -x /tmp/wildfly_install.sh 2> /tmp/wfoutput.txt'
  action :run
end

#bash resolts to console
ruby_block "Results" do
  only_if { ::File.exists?(results) }
  block do
    print "\n"
    File.open(results).each do |line|
      print line
    end
  end
end